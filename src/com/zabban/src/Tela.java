package com.zabban.src;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Window;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ThermometerPlot;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;
import org.snmp4j.smi.OID;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.naming.ContextNotEmptyException;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Calendar;
import java.awt.event.ActionEvent;
import java.awt.Rectangle;

public class Tela extends JFrame {
	private static boolean verificador=false;
	private JPanel contentPane;
	private JTextField ipMaquina;
	private JTextField nomeComunidade;
	private JTextField tempoAtualizacao;
	private JTextField nomeMaquina;
	private JTextField tempoLigada;
	private JTextField cpuusada;
	private JTextField ramTotal;
	private JTextField numProcessos;
	private JTextField numUsers;
	private JTextField consultaOid;
	//grafico iprecebido
	private DefaultXYDataset dsIpRecebido = new DefaultXYDataset();
	private DefaultCategoryDataset datasetIpRecebido = new DefaultCategoryDataset();
	private JFreeChart chartIpRecebido = ChartFactory.createLineChart("Datagramas IP Recebido",
            "Tempo", "Qtd. Datagramas", datasetIpRecebido, PlotOrientation.VERTICAL, true, true,
            false);
	private ChartPanel cpIpRecebido = new ChartPanel(chartIpRecebido);
	//grafico ipenviado
	private DefaultXYDataset dsIpEnviado = new DefaultXYDataset();
	private DefaultCategoryDataset datasetIpEnviado = new DefaultCategoryDataset();
	private JFreeChart chartIpEnviado = ChartFactory.createLineChart("Datagramas IP Enviado",
            "Tempo", "Qtd. Datagramas", datasetIpEnviado, PlotOrientation.VERTICAL, true, true,
            false);
	private ChartPanel cpIpEnviado = new ChartPanel(chartIpEnviado);
	
	//tcprecepido
	private DefaultXYDataset dsTcpRecebido = new DefaultXYDataset();
	private DefaultCategoryDataset datasetTcpRecebido = new DefaultCategoryDataset();
	private JFreeChart chartTcpRecebido = ChartFactory.createLineChart("Tcp Recebido",
            "Tempo", "Qtd. Pacotes", datasetTcpRecebido, PlotOrientation.VERTICAL, true, true,
            false);
	private ChartPanel cpTcpRecebido = new ChartPanel(chartTcpRecebido);
	//tcpenviado
	private DefaultXYDataset dsTcpEnviado = new DefaultXYDataset();
	private DefaultCategoryDataset datasetTcpEnviado = new DefaultCategoryDataset();
	private JFreeChart chartTcpEnviado = ChartFactory.createLineChart("TCP Enviado",
            "Tempo", "Qtd. Pacotes", datasetTcpEnviado, PlotOrientation.VERTICAL, true, true,
            false);
	private ChartPanel cpTcpEnviado = new ChartPanel(chartTcpEnviado);
	
	//udprecepido
	private DefaultXYDataset dsUdpRecebido = new DefaultXYDataset();
	private DefaultCategoryDataset datasetUdpRecebido = new DefaultCategoryDataset();
	private JFreeChart chartUdpRecebido = ChartFactory.createLineChart("Udp Recebido",
            "Tempo", "Qtd. Pacotes", datasetUdpRecebido, PlotOrientation.VERTICAL, true, true,
            false);
	private ChartPanel cpUdpRecebido = new ChartPanel(chartUdpRecebido);
	//udpenviado
	private DefaultXYDataset dsUdpEnviado = new DefaultXYDataset();
	private DefaultCategoryDataset datasetUdpEnviado = new DefaultCategoryDataset();
	private JFreeChart chartUdpEnviado = ChartFactory.createLineChart("UdP Enviado",
            "Tempo", "Qtd. Pacotes", datasetUdpEnviado, PlotOrientation.VERTICAL, true, true,
            false);
	private ChartPanel cpUdpEnviado = new ChartPanel(chartUdpEnviado);
	private JTextField limTcpMaxRecebido;
	private JTextField limTcpMinRecebido;
	private JTextField limTcpMaxEnviado;
	private JTextField limTcpMinEnviado;
	private JTextField limIpMinRecebido;
	private JTextField limIpMaxRecebido;
	private JTextField limIpMinEnviado;
	private JTextField limIpMaxEnviado;
	private JTextField limUdpMinRecebido;
	private JTextField limUdpMaxRecebido;
	private JTextField limUdpMinEnviado;
	private JTextField limUdpMaxEnviado;
	private boolean verIpRecebido = false;
	private boolean verTcpRecebido = false;
	private boolean verUdpRecebido = false;
	private boolean verIpEnviado = false;
	private boolean verTcpEnviado = false;
	private boolean verUdpEnviado
	= false;
	private JTextField getoid;
	private JTextField oidretornado;
	private JTextField usolink;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
						Tela frame = new Tela();
						frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Tela() {
		setTitle("Zabban");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1497, 1094);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblIpDaMquina = new JLabel("IP da M\u00E1quina:");
		lblIpDaMquina.setBounds(10, 11, 78, 14);
		contentPane.add(lblIpDaMquina);
		
		JLabel lblComunidade = new JLabel("Comunidade:");
		lblComunidade.setBounds(196, 8, 96, 14);
		contentPane.add(lblComunidade);
		
		JLabel lblTempos = new JLabel("Tempo (s):");
		lblTempos.setBounds(393, 8, 78, 14);
		contentPane.add(lblTempos);
		
		ipMaquina = new JTextField();
		ipMaquina.setText("127.0.0.1");
		ipMaquina.setBounds(92, 8, 86, 20);
		contentPane.add(ipMaquina);
		ipMaquina.setColumns(10);
		
		nomeComunidade = new JTextField();
		nomeComunidade.setText("public");
		nomeComunidade.setBounds(294, 8, 86, 20);
		contentPane.add(nomeComunidade);
		nomeComunidade.setColumns(10);
		
		tempoAtualizacao = new JTextField();
		tempoAtualizacao.setText("5");
		tempoAtualizacao.setBounds(473, 8, 24, 20);
		contentPane.add(tempoAtualizacao);
		tempoAtualizacao.setColumns(10);
		
		JButton btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int tempo = Integer.parseInt(tempoAtualizacao.getText());
				verificador=false;
				new Thread() {
					
					@Override
				    public void run() {
						while (verificador==false) {
							preencheTela();
							try {
								Thread.sleep((tempo-3)*1000);
							} catch (InterruptedException e) {
								System.out.println("Problema com o tempo oremos");
							}
						}	
					}
				}.start();
			}

		});
		btnStart.setBounds(547, 7, 89, 23);
		contentPane.add(btnStart);
		
		JButton btnStop = new JButton("Stop");
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verificador=true;
			}
		});
		btnStop.setBounds(658, 7, 89, 23);
		contentPane.add(btnStop);
		
		JLabel lblNomeDaMquina = new JLabel("Nome da m\u00E1quina:");
		lblNomeDaMquina.setBounds(10, 37, 132, 14);
		contentPane.add(lblNomeDaMquina);
		
		JLabel lblTempoLigada = new JLabel("Tempo ligada:");
		lblTempoLigada.setBounds(41, 63, 112, 14);
		contentPane.add(lblTempoLigada);
		
		nomeMaquina = new JTextField();
		nomeMaquina.setBounds(149, 34, 183, 20);
		contentPane.add(nomeMaquina);
		nomeMaquina.setColumns(10);
		
		tempoLigada = new JTextField();
		tempoLigada.setBounds(149, 57, 183, 20);
		contentPane.add(tempoLigada);
		tempoLigada.setColumns(10);
		
		cpuusada = new JTextField();
		cpuusada.setColumns(10);
		cpuusada.setBounds(535, 56, 183, 20);
		contentPane.add(cpuusada);
		
		ramTotal = new JTextField();
		ramTotal.setColumns(10);
		ramTotal.setBounds(535, 33, 183, 20);
		contentPane.add(ramTotal);
		
		JLabel lblMemRamUsada = new JLabel("Uso CPU:");
		lblMemRamUsada.setBounds(462, 59, 93, 14);
		contentPane.add(lblMemRamUsada);
		
		JLabel lblMemRamTotal = new JLabel("Mem RAM Total:");
		lblMemRamTotal.setBounds(414, 34, 112, 14);
		contentPane.add(lblMemRamTotal);
		
		numProcessos = new JTextField();
		numProcessos.setColumns(10);
		numProcessos.setBounds(149, 108, 183, 20);
		contentPane.add(numProcessos);
		
		numUsers = new JTextField();
		numUsers.setColumns(10);
		numUsers.setBounds(149, 85, 183, 20);
		contentPane.add(numUsers);
		
		JLabel lblNDeProcessos = new JLabel("N\u00BA de processos:");
		lblNDeProcessos.setBounds(22, 110, 168, 14);
		contentPane.add(lblNDeProcessos);
		
		JLabel lblNDeUsurios = new JLabel("N\u00BA de Usu\u00E1rios:");
		lblNDeUsurios.setBounds(28, 89, 114, 14);
		contentPane.add(lblNDeUsurios);
		cpIpRecebido.setBounds(new Rectangle(10, 168, 655, 210));
		contentPane.add(cpIpRecebido);
		
		JPanel panelIpRecebido = new JPanel();
		cpIpRecebido.add(panelIpRecebido);
		cpIpEnviado.setBounds(new Rectangle(677, 168, 655, 210));
		contentPane.add(cpIpEnviado);
		//contentPane.add(panelIpRecebido);
		//panelIpRecebido.add(cpIpRecebido);
		
		
		JPanel panelIpEnviado = new JPanel();
		cpIpEnviado.add(panelIpEnviado);
		cpTcpEnviado.setBounds(new Rectangle(677, 390, 655, 210));
		contentPane.add(cpTcpEnviado);
		//panelIpEnviado.add(cpIpEnviado);
		
		
		JPanel panelTcpEnviado = new JPanel();
		cpTcpEnviado.add(panelTcpEnviado);
		cpTcpRecebido.setBounds(new Rectangle(10, 390, 655, 210));
		contentPane.add(cpTcpRecebido);
		
		JPanel panelTcpRecebido = new JPanel();
		cpTcpRecebido.add(panelTcpRecebido);
		cpUdpEnviado.setBounds(new Rectangle(677, 619, 655, 210));
		contentPane.add(cpUdpEnviado);
		
		JPanel panelUdpEnviado = new JPanel();
		cpUdpEnviado.add(panelUdpEnviado);
		cpUdpRecebido.setBounds(new Rectangle(10, 619, 655, 210));
		contentPane.add(cpUdpRecebido);
		
		JPanel panelUdpRecebido = new JPanel();
		cpUdpRecebido.add(panelUdpRecebido);
		
		limTcpMaxRecebido = new JTextField();
		limTcpMaxRecebido.setText("0");
		limTcpMaxRecebido.setBounds(1379, 643-217, 86, 20);
		contentPane.add(limTcpMaxRecebido);
		limTcpMaxRecebido.setColumns(10);
		
		limTcpMinRecebido = new JTextField();
		limTcpMinRecebido.setText("0");
		limTcpMinRecebido.setColumns(10);
		limTcpMinRecebido.setBounds(1379, 612-217, 86, 20);
		contentPane.add(limTcpMinRecebido);
		
		JButton btnLimTcpRecebido = new JButton("Aplicar!");
		btnLimTcpRecebido.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verTcpRecebido=true;
			}
		});
		btnLimTcpRecebido.setBounds(1379, 674-217, 89, 23);
		contentPane.add(btnLimTcpRecebido);
		
		JButton btnLimTcpEnviado = new JButton("Aplicar!");
		btnLimTcpEnviado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verTcpEnviado=true;
			}
		});
		btnLimTcpEnviado.setBounds(1379, 764-217, 89, 23);
		contentPane.add(btnLimTcpEnviado);
		
		limTcpMaxEnviado = new JTextField();
		limTcpMaxEnviado.setText("0");
		limTcpMaxEnviado.setColumns(10);
		limTcpMaxEnviado.setBounds(1379, 733-217, 86, 20);
		contentPane.add(limTcpMaxEnviado);
		
		limTcpMinEnviado = new JTextField();
		limTcpMinEnviado.setText("0");
		limTcpMinEnviado.setColumns(10);
		limTcpMinEnviado.setBounds(1379, 702-217, 86, 20);
		contentPane.add(limTcpMinEnviado);
		
		limIpMinRecebido = new JTextField();
		limIpMinRecebido.setText("0");
		limIpMinRecebido.setColumns(10);
		limIpMinRecebido.setBounds(1379, 390-217, 86, 20);
		contentPane.add(limIpMinRecebido);
		
		limIpMaxRecebido = new JTextField();
		limIpMaxRecebido.setText("0");
		limIpMaxRecebido.setColumns(10);
		limIpMaxRecebido.setBounds(1379, 421-217, 86, 20);
		contentPane.add(limIpMaxRecebido);
		
		JButton btinLimIpRecebido = new JButton("Aplicar!");
		btinLimIpRecebido.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				verIpRecebido=true;
			}
		});
		btinLimIpRecebido.setBounds(1379, 452-217, 89, 23);
		contentPane.add(btinLimIpRecebido);
		
		limIpMinEnviado = new JTextField();
		limIpMinEnviado.setText("0");
		limIpMinEnviado.setColumns(10);
		limIpMinEnviado.setBounds(1382, 480-217, 86, 20);
		contentPane.add(limIpMinEnviado);
		
		JButton btnLimIpEnviado = new JButton("Aplicar!");
		btnLimIpEnviado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verIpEnviado=true;
			}
		});
		btnLimIpEnviado.setBounds(1379, 542-217, 89, 23);
		contentPane.add(btnLimIpEnviado);
		
		limIpMaxEnviado = new JTextField();
		limIpMaxEnviado.setText("0");
		limIpMaxEnviado.setColumns(10);
		limIpMaxEnviado.setBounds(1379, 511-217, 86, 20);
		contentPane.add(limIpMaxEnviado);
		
		limUdpMinRecebido = new JTextField();
		limUdpMinRecebido.setText("0");
		limUdpMinRecebido.setColumns(10);
		limUdpMinRecebido.setBounds(1376, 844-217, 86, 20);
		contentPane.add(limUdpMinRecebido);
		
		limUdpMaxRecebido = new JTextField();
		limUdpMaxRecebido.setText("0");
		limUdpMaxRecebido.setColumns(10);
		limUdpMaxRecebido.setBounds(1376, 875-217, 86, 20);
		contentPane.add(limUdpMaxRecebido);
		
		JButton btnLimUdpRecebido = new JButton("Aplicar!");
		btnLimUdpRecebido.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verUdpRecebido=true;
			}
		});
		btnLimUdpRecebido.setBounds(1376, 906-217, 89, 23);
		contentPane.add(btnLimUdpRecebido);
		
		limUdpMinEnviado = new JTextField();
		limUdpMinEnviado.setText("0");
		limUdpMinEnviado.setColumns(10);
		limUdpMinEnviado.setBounds(1376, 934-217, 86, 20);
		contentPane.add(limUdpMinEnviado);
		
		limUdpMaxEnviado = new JTextField();
		limUdpMaxEnviado.setText("0");
		limUdpMaxEnviado.setColumns(10);
		limUdpMaxEnviado.setBounds(1376, 965-217, 86, 20);
		contentPane.add(limUdpMaxEnviado);
		
		JButton btnLimUdpEnviado = new JButton("Aplicar!");
		btnLimUdpEnviado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verUdpEnviado=true;
			}
		});
		btnLimUdpEnviado.setBounds(1376, 996-217, 89, 23);
		contentPane.add(btnLimUdpEnviado);
		
		JLabel lblInformarOid = new JLabel("Informar O.I.D:");
		lblInformarOid.setBounds(424, 85, 104, 14);
		contentPane.add(lblInformarOid);
		
		getoid = new JTextField();
		getoid.setBounds(535, 81, 183, 20);
		contentPane.add(getoid);
		getoid.setColumns(10);
		
		JButton btnConsultar = new JButton("REFRESH");
		btnConsultar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				preencheTela();
			}
		});
		
		btnConsultar.setBounds(414, 106, 93, 23);
		contentPane.add(btnConsultar);
		
		oidretornado = new JTextField();
		oidretornado.setBounds(535, 108, 183, 20);
		contentPane.add(oidretornado);
		oidretornado.setColumns(10);
		
		JLabel lbllink = new JLabel("Uso link:");
		lbllink.setBounds(67, 136, 75, 14);
		contentPane.add(lbllink);
		
		usolink = new JTextField();
		usolink.setBounds(149, 133, 183, 20);
		contentPane.add(usolink);
		usolink.setColumns(10);
		
	}
	/*private static XYDataset createDataset(double[][] data) {

        //DefaultXYDataset ds = new DefaultXYDataset();

        double[][] data = { {0.1, 0.2, 0.3}, {1, 2, 3} };

        ds.addSeries("series1", data);

        return ds;
    }*/
	
	private void preencheTela() {
		// começo
		Calendar data = Calendar.getInstance();
		int hora = data.get(Calendar.HOUR_OF_DAY); 
		int min = data.get(Calendar.MINUTE);
		int seg = data.get(Calendar.SECOND);
		
		SNMPManager con = new SNMPManager("udp:"+ipMaquina.getText()+"/161", nomeComunidade.getText());
        try {
            con.start();
        } catch (Exception ex) {
        	System.out.println("Erro para começar o programa!");
        }
        
        //while(verificador==false) {
        
        //
        String sysName="";
        try {
            sysName = con.getAsString(new OID(".1.3.6.1.2.1.1.5.0"));
            System.out.println("Sysname: " + sysName);
            nomeMaquina.setText(sysName);
            nomeMaquina.setText(sysName);
        } catch (Exception ex) {
        	System.out.println("Erro para descobrir o sysName");
        	} 
        String sysUpTime="";
        try {
            sysUpTime = con.getAsString(new OID(".1.3.6.1.2.1.1.3.0"));
            System.out.println("sysUpTime: "+sysUpTime);
            tempoLigada.setText(sysUpTime);
        } catch (Exception ex) {
        	System.out.println("Erro para descobrir o sysUpTime");
        }
        
        String memTotal="";
        try {
            memTotal = con.getAsString(new OID(".1.3.6.1.2.1.25.2.2.0"));
            System.out.println("Memtotal: " + memTotal);
            ramTotal.setText(memTotal);
        } catch (Exception ex) {
        	System.out.println("Erro para mem Total");
        	} 
        String memUsada="";
        try {
        	String nucleo1 = con.getAsString(new OID(".1.3.6.1.2.1.25.3.3.1.2.196608"));
        	String nucleo2 = con.getAsString(new OID(".1.3.6.1.2.1.25.3.3.1.2.196609"));
        	String nucleo3 = con.getAsString(new OID(".1.3.6.1.2.1.25.3.3.1.2.196610"));
        	String nucleo4 = con.getAsString(new OID(".1.3.6.1.2.1.25.3.3.1.2.196611"));
        	System.out.println("Nucleo1:" + nucleo1);
        	int mediacpu = Integer.parseInt(nucleo1) + Integer.parseInt(nucleo2)+ Integer.parseInt(nucleo3) + Integer.parseInt(nucleo4);
        	mediacpu = mediacpu/4;
        	
            System.out.println("memUsada: "+memUsada);
            cpuusada.setText(Integer.toString(mediacpu)+ "%");
        } catch (Exception ex) {
        	System.out.println("Erro para descobrir o memUsada");
        }
        
        String numeUsers="";
        try {
        	numeUsers = con.getAsString(new OID(".1.3.6.1.2.1.25.1.5.0"));
            System.out.println("numUsers: " + numeUsers);
            numUsers.setText(numeUsers);
        } catch (Exception ex) {
        	System.out.println("Erro para numUsers");
        	} 
        String numeProcessos="";
        try {
        	numeProcessos = con.getAsString(new OID(".1.3.6.1.2.1.25.1.6.0"));
            System.out.println("numProcessos: "+numeProcessos);
            numProcessos.setText(numeProcessos);
        } catch (Exception ex) {
        	System.out.println("Erro para descobrir o numProcessos");
        }
        
        
        String oidConsultado = "";
        try {
        	oidConsultado = con.getAsString(new OID(getoid.getText()));
            System.out.println("OidConsultado: "+consultaOid);
            oidretornado.setText(oidConsultado);
        } catch (Exception ex) {
        	System.out.println("Erro para descobrir o consultaOid");
        }
        
        
        String linkusado = "";
        try {
        	oidConsultado = con.getAsString(new OID(" "));
            System.out.println("OidConsultado: "+consultaOid);
            oidretornado.setText(oidConsultado);
        } catch (Exception ex) {
        	System.out.println("Erro para descobrir o consultaOid");
        }
        
        
        
        
        //parte chata
        
        String dataIpRecebido="";
        try {
        	dataIpRecebido = con.getAsString(new OID(".1.3.6.1.2.1.4.3.0"));
            System.out.println("dataIpRecebido: "+dataIpRecebido);
            datasetIpRecebido.addValue(Integer.parseInt(dataIpRecebido), "maximo", Integer.toString(hora)+":"+Integer.toString(min)+":"+Integer.toString(seg));
            if (verIpRecebido==true) {
            	if (Integer.parseInt(dataIpRecebido) > Integer.parseInt(limIpMaxRecebido.getText())) {
            		JOptionPane.showMessageDialog(null,"Limite IP recebido m�ximo atingido!");
            	} else if (Integer.parseInt(dataIpRecebido) < Integer.parseInt(limIpMinRecebido.getText())){
            		JOptionPane.showMessageDialog(null,"Limite IP recebido m�nimo atingido!");
            	}
            }
        } catch (Exception ex) {
        	System.out.println("Erro para descobrir o dataIpRecebido");
        }
        
        String dataIpEncaminhado="";
        try {
        	dataIpEncaminhado = con.getAsString(new OID(".1.3.6.1.2.1.4.10.0"));
            System.out.println("ipEncaminhado: "+dataIpEncaminhado);
            datasetIpEnviado.addValue(Integer.parseInt(dataIpEncaminhado), "maximo", Integer.toString(hora)+":"+Integer.toString(min)+":"+Integer.toString(seg));
            if (verIpEnviado==true) {
            	if (Integer.parseInt(dataIpEncaminhado) > Integer.parseInt(limIpMaxEnviado.getText())) {
            		JOptionPane.showMessageDialog(null,"Limite IP enviado m�ximo atingido!");
            	} else if (Integer.parseInt(dataIpEncaminhado) < Integer.parseInt(limIpMinEnviado.getText())){
            		JOptionPane.showMessageDialog(null,"Limite IP enviado m�nimo atingido!");
            	}
            }
        } catch (Exception ex) {
        	System.out.println("Erro para descobrir o dataIpEncaminhado");
        }
        
        String pacotesTcpRecebido="";
        try {
        	pacotesTcpRecebido = con.getAsString(new OID(".1.3.6.1.2.1.6.10.0"));
            System.out.println("pacotesTcpRecebido: "+pacotesTcpRecebido);
            datasetTcpRecebido.addValue(Integer.parseInt(pacotesTcpRecebido), "maximo", Integer.toString(hora)+":"+Integer.toString(min)+":"+Integer.toString(seg));
            if (verTcpRecebido==true) {
            	if (Integer.parseInt(pacotesTcpRecebido) > Integer.parseInt(limTcpMaxRecebido.getText())) {
            		JOptionPane.showMessageDialog(null,"Limite TCP recebido m�ximo atingido!");
            	} else if (Integer.parseInt(pacotesTcpRecebido) < Integer.parseInt(limTcpMinRecebido.getText())){
            		JOptionPane.showMessageDialog(null,"Limite TCP recebido m�nimo atingido!");
            	}
            }
            //numProcessos.setText(numeProcessos);
        } catch (Exception ex) {
        	System.out.println("Erro para descobrir o pacotesTcpRecebido");
        }
        
        String pacotesTcpEnviado="";
        try {
        	pacotesTcpEnviado = con.getAsString(new OID(".1.3.6.1.2.1.6.11.0"));
            System.out.println("pacotesTcpEnviado: "+pacotesTcpEnviado);
            datasetTcpEnviado.addValue(Integer.parseInt(pacotesTcpEnviado), "maximo", Integer.toString(hora)+":"+Integer.toString(min)+":"+Integer.toString(seg));
            if (verTcpEnviado==true) {
            	if (Integer.parseInt(pacotesTcpEnviado) > Integer.parseInt(limTcpMaxEnviado.getText())) {
            		JOptionPane.showMessageDialog(null,"Limite TCP enviado m�ximo atingido!");
            	} else if (Integer.parseInt(pacotesTcpEnviado) < Integer.parseInt(limTcpMinEnviado.getText())){
            		JOptionPane.showMessageDialog(null,"Limite TCP enviado m�nimo atingido!");
            	}
            }
        } catch (Exception ex) {
        	System.out.println("Erro para descobrir o pacotesTcpEnviado");
        }
        
        String pacotesUdpRecebido="";
        try {
        	pacotesUdpRecebido = con.getAsString(new OID(".1.3.6.1.2.1.7.1.0"));
            System.out.println("pacotesUdpRecebido: "+pacotesUdpRecebido);
            datasetUdpRecebido.addValue(Integer.parseInt(pacotesUdpRecebido), "maximo", Integer.toString(hora)+":"+Integer.toString(min)+":"+Integer.toString(seg));
            if (verUdpRecebido==true) {
            	if (Integer.parseInt(pacotesUdpRecebido) > Integer.parseInt(limUdpMaxRecebido.getText())) {
            		JOptionPane.showMessageDialog(null,"Limite UDP recebido m�ximo atingido!");
            	} else if (Integer.parseInt(pacotesUdpRecebido) < Integer.parseInt(limUdpMinRecebido.getText())){
            		JOptionPane.showMessageDialog(null,"Limite UDP recebido m�nimo atingido!");
            	}
            }
        } catch (Exception ex) {
        	System.out.println("Erro para descobrir o pacotesTcpRecebido");
        }
        
        String pacotesUdpEnviado="";
        try {
        	pacotesUdpEnviado = con.getAsString(new OID(".1.3.6.1.2.1.7.4.0"));
            System.out.println("pacotesUdpEnviado: "+pacotesUdpEnviado);
            datasetUdpEnviado.addValue(Integer.parseInt(pacotesUdpEnviado), "maximo", Integer.toString(hora)+":"+Integer.toString(min)+":"+Integer.toString(seg));
            if (verUdpEnviado==true) {
            	if (Integer.parseInt(pacotesUdpEnviado) > Integer.parseInt(limUdpMaxEnviado.getText())) {
            		JOptionPane.showMessageDialog(null,"Limite UdP enviado m�ximo atingido!");
            	} else if (Integer.parseInt(pacotesUdpEnviado) < Integer.parseInt(limUdpMinEnviado.getText())){
            		JOptionPane.showMessageDialog(null,"Limite UDP enviado m�nimo atingido!");
            	}
            }
        } catch (Exception ex) {
        	System.out.println("Erro para descobrir o pacotesUdpEnviado");
        }
        //usolink
        String taxalink="";
        try {
        	String ifinocta = con.getAsString(new OID(".1.3.6.1.2.1.2.2.1.10.2"));
        	String ifoutocta = con.getAsString(new OID(".1.3.6.1.2.1.2.2.1.16.2"));
        	Thread.sleep(3*1000);
        	String ifinoctb = con.getAsString(new OID(".1.3.6.1.2.1.2.2.1.10.2"));
        	String ifoutoctb = con.getAsString(new OID(".1.3.6.1.2.1.2.2.1.16.2"));
        
        	
        	float totalbytes = (Integer.parseInt(ifinoctb) - Integer.parseInt(ifinocta)) + (Integer.parseInt(ifoutoctb) - Integer.parseInt(ifoutocta));
        	
        	totalbytes = totalbytes/3;
        	totalbytes = totalbytes*8;
        	
        	String ifspeed = con.getAsString(new OID(".1.3.6.1.2.1.2.2.1.5.2"));
        	
        	double taxa = totalbytes/Integer.parseInt(ifspeed);
        	String resultado = String.format("%.2f", taxa);
        	//String resultado = Double.toString(taxa);
        	usolink.setText(resultado + "%");
        	System.out.println(taxa);
        	
        } catch (InterruptedException ex) {
        	System.out.println("Erro para descobrir o pacotesUdpEnviado");
        }
        
	}
}
